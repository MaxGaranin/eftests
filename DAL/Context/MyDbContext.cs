﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using DAL.Classes;

namespace DAL.Context
{
    public class MyDbContext : DbContext
    {
        private readonly DataStore _ds;

        internal MyDbContext(string fileName, DataStore dataStore) : base("Data Source=" + fileName)
        {
            _ds = dataStore;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            CreateMappings(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        private void CreateMappings(DbModelBuilder modelBuilder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                .Where(type => !String.IsNullOrEmpty(type.Namespace))
                .Where(type => type.BaseType != null && 
                    type.BaseType.IsGenericType && 
                    type.BaseType.GetGenericTypeDefinition() == typeof (EntityTypeConfiguration<>));

            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
        }

        public override int SaveChanges()
        {
            _ds.OnSaveChanges(this);
            return base.SaveChanges();
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Survey> Surveys { get; set; }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<OfficeAssignment> OfficeAssignments { get; set; }

        public DbSet<NFluidModelBank> NFluidModelBanks { get; set; }
        public DbSet<NFluidModelGroup> NFluidModelGroups { get; set; }
        public DbSet<NFluidModel> NFluidModels { get; set; }
        public DbSet<NFluidCalibration> NFluidCalibrations { get; set; }
    }
}
