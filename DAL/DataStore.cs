﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using DAL.Context;

namespace DAL
{
    public class DataStore : IDisposable
    {
        private string _dbName;

        #region Singleton

        private DataStore()
        {
        }

        private static DataStore _instance;

        public static DataStore Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DataStore();
                    _instance.New();
                }
                return _instance;
            }
        }

        #endregion

        public MyDbContext GetContext()
        {
            return new MyDbContext(_dbName, this);
        }

        public void Dispose()
        {
            if (File.Exists(_dbName)) File.Delete(_dbName);
        }

        #region DataBase FileOperations

        public void New()
        {
            Dispose();

            _dbName = CreateTempDbName();
            GetContext().Database.CreateIfNotExists();
        }

        private static string CreateTempDbName()
        {
            var name = Path.ChangeExtension(Path.GetTempFileName(), "sdf");
            return name;
        }

        public void Open(string fileName)
        {
            CopyBase(fileName, _dbName);
        }

        public void Save(string fileName)
        {
            CopyBase(_dbName, fileName);
        }

        private void CopyBase(string from, string to)
        {
            if (File.Exists(from)) File.Copy(from, to, true);
        }

        #endregion

        #region EventActions

        private readonly Dictionary<Type, Dictionary<EntityState, IEventContainer>> _handlers =
            new Dictionary<Type, Dictionary<EntityState, IEventContainer>>();

        public void OnSaveChanges(DbContext dbc)
        {
            foreach (var entry in dbc.ChangeTracker.Entries())
            {
                Dictionary<EntityState, IEventContainer> stateHandlers;
                if (!_handlers.TryGetValue(entry.Entity.GetType(), out stateHandlers)) continue;
                IEventContainer handler;
                if (stateHandlers.TryGetValue(entry.State, out handler)) handler.OnEvent(entry.Entity);
            }
        }

        public void AddHandler<T>(EntityState state, Action<T> handler) where T : class
        {
            lock (_handlers)
            {
                Dictionary<EntityState, IEventContainer> stateHandlers;
                if (!_handlers.TryGetValue(typeof (T), out stateHandlers))
                {
                    stateHandlers = new Dictionary<EntityState, IEventContainer>();
                    _handlers.Add(typeof (T), stateHandlers);
                }
                IEventContainer stateHandler;
                if (!stateHandlers.TryGetValue(state, out stateHandler))
                {
                    stateHandler = new EventContainer<T>();
                    stateHandlers.Add(state, stateHandler);
                }
                ((EventContainer<T>) stateHandler).Event += handler;
            }
        }

        public void RemoveHandler<T>(EntityState state, Action<T> handler) where T : class
        {
            lock (_handlers)
            {
                Dictionary<EntityState, IEventContainer> stateHandlers;
                if (!_handlers.TryGetValue(typeof (T), out stateHandlers)) return;
                IEventContainer stateHandler;
                if (!stateHandlers.TryGetValue(state, out stateHandler)) return;
                ((EventContainer<T>) stateHandler).Event -= handler;
            }
        }

        private interface IEventContainer
        {
            void OnEvent(object sender);
        }

        private class EventContainer<T> : IEventContainer
        {
            public event Action<T> Event;

            public void OnEvent(object sender)
            {
                var handler = Event;
                if (handler != null) handler((T) sender);
            }
        }

        #endregion
    }
}