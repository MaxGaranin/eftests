﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DAL.Classes.Enums.CalibrationGroups;
using DAL.Classes.Enums.CorrelationTypes;

namespace DAL.Classes
{
    public class NFluidModel : INotifyPropertyChanged
    {
        #region Fields

        private double _oilSg;
        private double _waterSg;
        private double _gasSg;
        private double _wcCriticalPercent;
        private double _gor;

        private LiquidViscosityCorrelationType _liquidViscosityCorrelationType;
        private DeadOilViscosityCorrelationType _deadOilViscosityCorrelationType;
        private OilFvfCorrelationType _oilFvfCorrelationType;
        private BubblePointPressureCorrelationType _bubblePointPressureCorrelationType;
        private SaturatedOilFvfCorrelationType _saturatedOilFvfCorrelationType;
        private GasZFactorCorrelationType _gasZFactorCorrelationType;
        private GasViscosityCorrelationType _gasViscosityCorrelationType;
        private GasFvfCorrelationType _gasFvfCorrelationType;
        private GasSaturationCorrelationType _gasSaturationCorrelationType;
        private UndersaturatedOilViscosityCorrelationType _undersaturatedOilViscosityCorrelationType;
        private SaturatedOilViscosityCorrelationType _saturatedOilViscosityCorrelationType;
        private bool _useAboveCalibrationGroup;
        private AboveCalibrationGroup _aboveCalibrationGroup;
        private NFluidCalibration _aboveOilDensityCalibration;
        private NFluidCalibration _aboveOilFvfCalibration;
        private NFluidCalibration _aboveUnsaturatedOilCompressibilityCalibration;
        private bool _useBelowCalibrationGroup;
        private BelowCalibrationGroup _belowCalibrationGroup;
        private NFluidCalibration _belowOilDensityCalibration;
        private NFluidCalibration _belowSaturatedOilFvfCalibration;
        private bool _useGasSaturationCalibration;
        private NFluidCalibration _gasSaturationCalibration;
        private bool _useSaturatedOilViscosityCalibration;
        private NFluidCalibration _saturatedOilViscosityCalibration;
        private bool _useGasViscosityCalibration;
        private NFluidCalibration _gasViscosityCalibration;
        private bool _useGasZFactorCalibration;
        private NFluidCalibration _gasZFactorCalibration;
        private double _deadOilViscosityUserDefinedParamT1;
        private double _deadOilViscosityUserDefinedParamV1;
        private double _deadOilViscosityUserDefinedParamT2;
        private double _deadOilViscosityUserDefinedParamV2;
        private double _liquidViscosityCommonParamWc;
        private double _liquidViscosityVolumeFractionParamWc;
        private double _liquidViscosityWoelflinParamWc;
        private string _name;
        private NFluidModelGroup _group;
        private string _description;
        private bool _isExistsInBank;

        #endregion

        #region Constructor

        public NFluidModel()
        {
            Id = Guid.NewGuid();
            InitCalibrations(this);
        }

        public static NFluidModel CreateDefaultModel(string name, bool existsInBank = false)
        {
            var model = new NFluidModel
            {
                Name = name,
                IsExistsInBank = existsInBank,
                OilSg = 1.0,
                WaterSg = 1.0,
                GasSg = 1.0,
                WcCriticalPercent = 1.0,
                Gor = 1.0,
                DeadOilViscosityUserDefinedParamT1 = 20.0,
                DeadOilViscosityUserDefinedParamV1 = 2.0,
                DeadOilViscosityUserDefinedParamT2 = 60.0,
                DeadOilViscosityUserDefinedParamV2 = 1.0,
                LiquidViscosityCommonParamWc = 0.6,
                LiquidViscosityVolumeFractionParamWc = 0.6,
                LiquidViscosityWoelflinParamWc = 0.6,
            };
            InitCalibrations(model);

            return model;
        }

        private static void InitCalibrations(NFluidModel fluidModel)
        {
            fluidModel.AboveOilDensityCalibration = new NFluidCalibration();
            fluidModel.AboveOilFvfCalibration = new NFluidCalibration();
            fluidModel.AboveUnsaturatedOilCompressibilityCalibration = new NFluidCalibration();
            fluidModel.GasSaturationCalibration = new NFluidCalibration();
            fluidModel.BelowOilDensityCalibration = new NFluidCalibration();
            fluidModel.BelowSaturatedOilFvfCalibration = new NFluidCalibration();
            fluidModel.SaturatedOilViscosityCalibration = new NFluidCalibration();
            fluidModel.GasViscosityCalibration = new NFluidCalibration();
            fluidModel.GasZFactorCalibration = new NFluidCalibration();
        }

        #endregion

        #region Main properties

        public Guid Id { get; set; }

        public Guid? GroupId { get; set; }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                RaisePropertyChanged("Name");
            }
        }

        public virtual NFluidModelGroup Group
        {
            get { return _group; }
            set
            {
                if (Equals(value, _group)) return;
                _group = value;
                RaisePropertyChanged("Group");
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value == _description) return;
                _description = value;
                RaisePropertyChanged("Description");
            }
        }

        public bool IsExistsInBank
        {
            get { return _isExistsInBank; }
            set
            {
                if (value.Equals(_isExistsInBank)) return;
                _isExistsInBank = value;
                RaisePropertyChanged("IsExistsInBank");
            }
        }

        #endregion

        #region Fluid properties

        /// <summary>
        /// Относительная плотность нефти 
        /// </summary>
        [DataMember]
        public double OilSg
        {
            get { return _oilSg; }
            set
            {
                if (_oilSg.Equals(value)) return;
                _oilSg = value;
                RaisePropertyChanged("OilSg");
            }
        }

        /// <summary>
        /// Относительная плотность воды
        /// </summary>
        [DataMember]
        public double WaterSg
        {
            get { return _waterSg; }
            set
            {
                if (_waterSg.Equals(value)) return;
                _waterSg = value;
                RaisePropertyChanged("WaterSg");
            }
        }

        /// <summary>
        /// Относительная плотность газа
        /// </summary>
        [DataMember]
        public double GasSg
        {
            get { return _gasSg; }
            set
            {
                if (_gasSg.Equals(value)) return;
                _gasSg = value;
                RaisePropertyChanged("GasSg");
            }
        }

        /// <summary>
        /// Критическая обводнённость 
        /// </summary>
        [DataMember]
        public double WcCriticalPercent
        {
            get { return _wcCriticalPercent; }
            set
            {
                if (_wcCriticalPercent.Equals(value)) return;
                _wcCriticalPercent = value;
                RaisePropertyChanged("WcCriticalPercent");
            }
        }

        /// <summary>
        /// Газовый фактор
        /// </summary>
        [DataMember]
        public double Gor
        {
            get { return _gor; }
            set
            {
                if (_gor.Equals(value)) return;
                _gor = value;
                RaisePropertyChanged("Gor");
            }
        }

        #endregion

        #region Correlations

        #region DeadOilViscosity

        public DeadOilViscosityCorrelationType DeadOilViscosityCorrelationType
        {
            get { return _deadOilViscosityCorrelationType; }
            set
            {
                if (value == _deadOilViscosityCorrelationType) return;
                _deadOilViscosityCorrelationType = value;
                RaisePropertyChanged("DeadOilViscosityCorrelationType");
            }
        }

        public double DeadOilViscosityUserDefinedParamT1
        {
            get { return _deadOilViscosityUserDefinedParamT1; }
            set
            {
                if (value.Equals(_deadOilViscosityUserDefinedParamT1)) return;
                _deadOilViscosityUserDefinedParamT1 = value;
                RaisePropertyChanged("DeadOilViscosityUserDefinedParamT1");
            }
        }

        public double DeadOilViscosityUserDefinedParamV1
        {
            get { return _deadOilViscosityUserDefinedParamV1; }
            set
            {
                if (value.Equals(_deadOilViscosityUserDefinedParamV1)) return;
                _deadOilViscosityUserDefinedParamV1 = value;
                RaisePropertyChanged("DeadOilViscosityUserDefinedParamV1");
            }
        }

        public double DeadOilViscosityUserDefinedParamT2
        {
            get { return _deadOilViscosityUserDefinedParamT2; }
            set
            {
                if (value.Equals(_deadOilViscosityUserDefinedParamT2)) return;
                _deadOilViscosityUserDefinedParamT2 = value;
                RaisePropertyChanged("DeadOilViscosityUserDefinedParamT2");
            }
        }

        public double DeadOilViscosityUserDefinedParamV2
        {
            get { return _deadOilViscosityUserDefinedParamV2; }
            set
            {
                if (value.Equals(_deadOilViscosityUserDefinedParamV2)) return;
                _deadOilViscosityUserDefinedParamV2 = value;
                RaisePropertyChanged("DeadOilViscosityUserDefinedParamV2");
            }
        }

        #endregion

        #region LiquidViscosity

        public LiquidViscosityCorrelationType LiquidViscosityCorrelationType
        {
            get { return _liquidViscosityCorrelationType; }
            set
            {
                if (value == _liquidViscosityCorrelationType) return;
                _liquidViscosityCorrelationType = value;
                RaisePropertyChanged("LiquidViscosityCorrelationType");
            }
        }

        public double LiquidViscosityCommonParamWc
        {
            get { return _liquidViscosityCommonParamWc; }
            set
            {
                if (value.Equals(_liquidViscosityCommonParamWc)) return;
                _liquidViscosityCommonParamWc = value;
                RaisePropertyChanged("LiquidViscosityCommonParamWc");
            }
        }

        public double LiquidViscosityVolumeFractionParamWc
        {
            get { return _liquidViscosityVolumeFractionParamWc; }
            set
            {
                if (value.Equals(_liquidViscosityVolumeFractionParamWc)) return;
                _liquidViscosityVolumeFractionParamWc = value;
                RaisePropertyChanged("LiquidViscosityVolumeFractionParamWc");
            }
        }

        public double LiquidViscosityWoelflinParamWc
        {
            get { return _liquidViscosityWoelflinParamWc; }
            set
            {
                if (value.Equals(_liquidViscosityWoelflinParamWc)) return;
                _liquidViscosityWoelflinParamWc = value;
                RaisePropertyChanged("LiquidViscosityWoelflinParamWc");
            }
        }

        #endregion

        #region Others

        public SaturatedOilViscosityCorrelationType SaturatedOilViscosityCorrelationType
        {
            get { return _saturatedOilViscosityCorrelationType; }
            set
            {
                if (value == _saturatedOilViscosityCorrelationType) return;
                _saturatedOilViscosityCorrelationType = value;
                RaisePropertyChanged("SaturatedOilViscosityCorrelationType");
            }
        }

        public UndersaturatedOilViscosityCorrelationType UndersaturatedOilViscosityCorrelationType
        {
            get { return _undersaturatedOilViscosityCorrelationType; }
            set
            {
                if (value == _undersaturatedOilViscosityCorrelationType) return;
                _undersaturatedOilViscosityCorrelationType = value;
                RaisePropertyChanged("UndersaturatedOilViscosityCorrelationType");
            }
        }

        public GasSaturationCorrelationType GasSaturationCorrelationType
        {
            get { return _gasSaturationCorrelationType; }
            set
            {
                if (value == _gasSaturationCorrelationType) return;
                _gasSaturationCorrelationType = value;
                RaisePropertyChanged("GasSaturationCorrelationType");
            }
        }

        public GasFvfCorrelationType GasFvfCorrelationType
        {
            get { return _gasFvfCorrelationType; }
            set
            {
                if (value == _gasFvfCorrelationType) return;
                _gasFvfCorrelationType = value;
                RaisePropertyChanged("GasFvfCorrelationType");
            }
        }

        public GasViscosityCorrelationType GasViscosityCorrelationType
        {
            get { return _gasViscosityCorrelationType; }
            set
            {
                if (value == _gasViscosityCorrelationType) return;
                _gasViscosityCorrelationType = value;
                RaisePropertyChanged("GasViscosityCorrelationType");
            }
        }

        public GasZFactorCorrelationType GasZFactorCorrelationType
        {
            get { return _gasZFactorCorrelationType; }
            set
            {
                if (value == _gasZFactorCorrelationType) return;
                _gasZFactorCorrelationType = value;
                RaisePropertyChanged("GasZFactorCorrelationType");
            }
        }

        public SaturatedOilFvfCorrelationType SaturatedOilFvfCorrelationType
        {
            get { return _saturatedOilFvfCorrelationType; }
            set
            {
                if (value == _saturatedOilFvfCorrelationType) return;
                _saturatedOilFvfCorrelationType = value;
                RaisePropertyChanged("SaturatedOilFvfCorrelationType");
            }
        }

        public BubblePointPressureCorrelationType BubblePointPressureCorrelationType
        {
            get { return _bubblePointPressureCorrelationType; }
            set
            {
                if (value == _bubblePointPressureCorrelationType) return;
                _bubblePointPressureCorrelationType = value;
                RaisePropertyChanged("BubblePointPressureCorrelationType");
            }
        }

        public OilFvfCorrelationType OilFvfCorrelationType
        {
            get { return _oilFvfCorrelationType; }
            set
            {
                if (value == _oilFvfCorrelationType) return;
                _oilFvfCorrelationType = value;
                RaisePropertyChanged("OilFvfCorrelationType");
            }
        }

        #endregion

        #endregion

        #region Calibrations

        #region AboveCalibrationGroup

        public bool UseAboveCalibrationGroup
        {
            get { return _useAboveCalibrationGroup; }
            set
            {
                if (value.Equals(_useAboveCalibrationGroup)) return;
                _useAboveCalibrationGroup = value;
                RaisePropertyChanged("UseAboveCalibrationGroup");
            }
        }

        public AboveCalibrationGroup AboveCalibrationGroup
        {
            get { return _aboveCalibrationGroup; }
            set
            {
                if (value == _aboveCalibrationGroup) return;
                _aboveCalibrationGroup = value;
                RaisePropertyChanged("AboveCalibrationGroup");
            }
        }

        public virtual NFluidCalibration AboveOilDensityCalibration
        {
            get { return _aboveOilDensityCalibration; }
            set
            {
                if (Equals(value, _aboveOilDensityCalibration)) return;
                _aboveOilDensityCalibration = value;
                RaisePropertyChanged("AboveOilDensityCalibration");
            }
        }

        public virtual NFluidCalibration AboveOilFvfCalibration
        {
            get { return _aboveOilFvfCalibration; }
            set
            {
                if (Equals(value, _aboveOilFvfCalibration)) return;
                _aboveOilFvfCalibration = value;
                RaisePropertyChanged("AboveOilFvfCalibration");
            }
        }

        public virtual NFluidCalibration AboveUnsaturatedOilCompressibilityCalibration
        {
            get { return _aboveUnsaturatedOilCompressibilityCalibration; }
            set
            {
                if (Equals(value, _aboveUnsaturatedOilCompressibilityCalibration)) return;
                _aboveUnsaturatedOilCompressibilityCalibration = value;
                RaisePropertyChanged("AboveUnsaturatedOilCompressibilityCalibration");
            }
        }

        #endregion

        #region BelowCalibrationGroup

        public bool UseBelowCalibrationGroup
        {
            get { return _useBelowCalibrationGroup; }
            set
            {
                if (value.Equals(_useBelowCalibrationGroup)) return;
                _useBelowCalibrationGroup = value;
                RaisePropertyChanged("UseBelowCalibrationGroup");
            }
        }

        public BelowCalibrationGroup BelowCalibrationGroup
        {
            get { return _belowCalibrationGroup; }
            set
            {
                if (value == _belowCalibrationGroup) return;
                _belowCalibrationGroup = value;
                RaisePropertyChanged("BelowCalibrationGroup");
            }
        }

        public virtual NFluidCalibration BelowOilDensityCalibration
        {
            get { return _belowOilDensityCalibration; }
            set
            {
                if (Equals(value, _belowOilDensityCalibration)) return;
                _belowOilDensityCalibration = value;
                RaisePropertyChanged("BelowOilDensityCalibration");
            }
        }

        public virtual NFluidCalibration BelowSaturatedOilFvfCalibration
        {
            get { return _belowSaturatedOilFvfCalibration; }
            set
            {
                if (Equals(value, _belowSaturatedOilFvfCalibration)) return;
                _belowSaturatedOilFvfCalibration = value;
                RaisePropertyChanged("BelowSaturatedOilFvfCalibration");
            }
        }

        #endregion

        #region Others

        public bool UseGasSaturationCalibration
        {
            get { return _useGasSaturationCalibration; }
            set
            {
                if (value.Equals(_useGasSaturationCalibration)) return;
                _useGasSaturationCalibration = value;
                RaisePropertyChanged("UseGasSaturationCalibration");
            }
        }

        public virtual NFluidCalibration GasSaturationCalibration
        {
            get { return _gasSaturationCalibration; }
            set
            {
                if (Equals(value, _gasSaturationCalibration)) return;
                _gasSaturationCalibration = value;
                RaisePropertyChanged("GasSaturationCalibration");
            }
        }

        public bool UseSaturatedOilViscosityCalibration
        {
            get { return _useSaturatedOilViscosityCalibration; }
            set
            {
                if (value.Equals(_useSaturatedOilViscosityCalibration)) return;
                _useSaturatedOilViscosityCalibration = value;
                RaisePropertyChanged("UseSaturatedOilViscosityCalibration");
            }
        }

        public virtual NFluidCalibration SaturatedOilViscosityCalibration
        {
            get { return _saturatedOilViscosityCalibration; }
            set
            {
                if (Equals(value, _saturatedOilViscosityCalibration)) return;
                _saturatedOilViscosityCalibration = value;
                RaisePropertyChanged("SaturatedOilViscosityCalibration");
            }
        }

        public bool UseGasViscosityCalibration
        {
            get { return _useGasViscosityCalibration; }
            set
            {
                if (value.Equals(_useGasViscosityCalibration)) return;
                _useGasViscosityCalibration = value;
                RaisePropertyChanged("UseGasViscosityCalibration");
            }
        }

        public virtual NFluidCalibration GasViscosityCalibration
        {
            get { return _gasViscosityCalibration; }
            set
            {
                if (Equals(value, _gasViscosityCalibration)) return;
                _gasViscosityCalibration = value;
                RaisePropertyChanged("GasViscosityCalibration");
            }
        }

        public bool UseGasZFactorCalibration
        {
            get { return _useGasZFactorCalibration; }
            set
            {
                if (value.Equals(_useGasZFactorCalibration)) return;
                _useGasZFactorCalibration = value;
                RaisePropertyChanged("UseGasZFactorCalibration");
            }
        }

        public virtual NFluidCalibration GasZFactorCalibration
        {
            get { return _gasZFactorCalibration; }
            set
            {
                if (Equals(value, _gasZFactorCalibration)) return;
                _gasZFactorCalibration = value;
                RaisePropertyChanged("GasZFactorCalibration");
            }
        }

        #endregion

        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return Name;
        }

        #endregion

        #region Clone

        public NFluidModel Clone(bool isCopyId = false)
        {
            var fluidModel = new NFluidModel
            {
                Id = this.Id,
                Name = this.Name,
                Group = this.Group,
                Description = this.Description,
                IsExistsInBank = this.IsExistsInBank,
                DeadOilViscosityCorrelationType = this.DeadOilViscosityCorrelationType,
                DeadOilViscosityUserDefinedParamT1 = this.DeadOilViscosityUserDefinedParamT1,
                DeadOilViscosityUserDefinedParamV1 = this.DeadOilViscosityUserDefinedParamV1,
                DeadOilViscosityUserDefinedParamT2 = this.DeadOilViscosityUserDefinedParamT2,
                DeadOilViscosityUserDefinedParamV2 = this.DeadOilViscosityUserDefinedParamV2,
                LiquidViscosityCorrelationType = this.LiquidViscosityCorrelationType,
                LiquidViscosityCommonParamWc = this.LiquidViscosityCommonParamWc,
                LiquidViscosityVolumeFractionParamWc = this.LiquidViscosityVolumeFractionParamWc,
                LiquidViscosityWoelflinParamWc = this.LiquidViscosityWoelflinParamWc,
                SaturatedOilViscosityCorrelationType = this.SaturatedOilViscosityCorrelationType,
                UndersaturatedOilViscosityCorrelationType = this.UndersaturatedOilViscosityCorrelationType,
                GasSaturationCorrelationType = this.GasSaturationCorrelationType,
                GasFvfCorrelationType = this.GasFvfCorrelationType,
                GasViscosityCorrelationType = this.GasViscosityCorrelationType,
                GasZFactorCorrelationType = this.GasZFactorCorrelationType,
                SaturatedOilFvfCorrelationType = this.SaturatedOilFvfCorrelationType,
                BubblePointPressureCorrelationType = this.BubblePointPressureCorrelationType,
                OilFvfCorrelationType = this.OilFvfCorrelationType,
                UseAboveCalibrationGroup = this.UseAboveCalibrationGroup,
                AboveCalibrationGroup = this.AboveCalibrationGroup,
                AboveOilDensityCalibration = this.AboveOilDensityCalibration.Clone(isCopyId),
                AboveOilFvfCalibration = this.AboveOilFvfCalibration.Clone(isCopyId),
                AboveUnsaturatedOilCompressibilityCalibration = this.AboveUnsaturatedOilCompressibilityCalibration.Clone(isCopyId),
                UseBelowCalibrationGroup = this.UseBelowCalibrationGroup,
                BelowCalibrationGroup = this.BelowCalibrationGroup,
                BelowOilDensityCalibration = this.BelowOilDensityCalibration.Clone(isCopyId),
                BelowSaturatedOilFvfCalibration = this.BelowOilDensityCalibration.Clone(isCopyId),
                UseGasSaturationCalibration = this.UseGasSaturationCalibration,
                GasSaturationCalibration = this.GasSaturationCalibration.Clone(isCopyId),
                UseSaturatedOilViscosityCalibration = this.UseSaturatedOilViscosityCalibration,
                SaturatedOilViscosityCalibration = this.SaturatedOilViscosityCalibration.Clone(isCopyId),
                UseGasViscosityCalibration = this.UseGasViscosityCalibration,
                GasViscosityCalibration = this.GasViscosityCalibration.Clone(isCopyId),
                UseGasZFactorCalibration = this.UseGasZFactorCalibration,
                GasZFactorCalibration = this.GasZFactorCalibration.Clone(isCopyId)
            };

            if (isCopyId) fluidModel.Id = this.Id;

            return fluidModel;
        }

        #endregion
    }
}