﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CalibrationGroups
{
    public enum AboveCalibrationGroup
    {
        [Description("Плотность нефти")]
        OilDensity,

        [Description("Объемный фактор нефти")]
        OilFvf,

        [Description("Сжимаемость недонасыщенной нефти")]
        UnsaturatedOilCompressibility
    }
}