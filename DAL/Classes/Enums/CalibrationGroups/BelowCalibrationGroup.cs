﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CalibrationGroups
{
    public enum BelowCalibrationGroup
    {
        [Description("Плотность нефти")]
        OilDensity,

        [Description("Объемный фактор насыщенной нефти")]
        SaturatedOilFvf
    }
}