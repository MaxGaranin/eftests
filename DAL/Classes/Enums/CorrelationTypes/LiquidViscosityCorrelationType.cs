﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CorrelationTypes
{
    public enum LiquidViscosityCorrelationType
    {
        [Description("По граничной обводненности")]
        Common,

        [Description("По отношению объемов")]
        VolumeFraction,

        [Description("Корреляция Woelflin для эмульсии")]
        Woelflin
    }
}