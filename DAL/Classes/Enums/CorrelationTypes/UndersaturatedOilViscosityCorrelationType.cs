﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CorrelationTypes
{
    public enum UndersaturatedOilViscosityCorrelationType
    {
        [Description("Vazquez Beggs")] 
        VazquezBeggs,

        [Description("Kouzel")] 
        Kouzel,

        [Description("Kartoatmodjo")] 
        Kartoatmodjo,

        [Description("Khan")] 
        Khan
    }
}