﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CorrelationTypes
{
    public enum BubblePointPressureCorrelationType
    {
        [Description("Lasater")]
        Lasater,

        [Description("Standing")]
        Standing,

        [Description("Vazquez Beggs")]
        VazquezBeggs,

        [Description("Glaso")]
        Glaso,

        [Description("Kartoatmodjo")]
        Kartoatmodjo
    }
}