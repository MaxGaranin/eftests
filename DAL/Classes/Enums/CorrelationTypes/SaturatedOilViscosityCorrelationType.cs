﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CorrelationTypes
{
    public enum SaturatedOilViscosityCorrelationType
    {
        [Description("Chew Connally")]
        ChewConnally,

        [Description("Beggs Robinson")]
        BeggsRobinson,

        [Description("Kartoatmodjo")]
        Kartoatmodjo,

        [Description("Khan")]
        Khan
    }
}