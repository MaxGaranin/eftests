﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CorrelationTypes
{
    public enum DeadOilViscosityCorrelationType
    {
        [Description("Beggs Robinson")]
        BeggsRobinson,

        [Description("Glaso")]
        Glaso,

        [Description("Kartoatmodjo")]
        Kartoatmodjo,

        [Description("Данные пользователя")]
        UserData
    }
}