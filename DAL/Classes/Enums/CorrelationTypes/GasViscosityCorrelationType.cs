﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CorrelationTypes
{
    public enum GasViscosityCorrelationType
    {
        [Description("Lee et al.")]
        LeeEtAl
    }
}