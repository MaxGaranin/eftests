﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CorrelationTypes
{
    public enum SaturatedOilFvfCorrelationType
    {
        [Description("Standing")]
        Standing,

        [Description("Vazquez Beggs")]
        VazquezBeggs,

        [Description("Kartoatmodjo")]
        Kartoatmodjo
    }
}