﻿using System.ComponentModel;

namespace DAL.Classes.Enums.CorrelationTypes
{
    public enum GasZFactorCorrelationType
    {
        [Description("Standing")]
        Standing,

        [Description("HallYarborough")]
        HallYarborough,

        [Description("Robinson et al.")]
        RobinsonEtAl
    }
}