﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace DAL.Classes
{
    public class NFluidModelBank : INotifyPropertyChanged
    {
        private NFluidModel _defaultFluidModel;
        private ObservableCollection<NFluidModelGroup> _fluidModelGroups;

        public NFluidModelBank()
        {
            Id = Guid.NewGuid();
            _fluidModelGroups = new ObservableCollection<NFluidModelGroup>();
        }

        #region Properties

        public Guid Id { get; set; }

        public virtual NFluidModel DefaultFluidModel
        {
            get { return _defaultFluidModel; }
            set
            {
                 if (Equals(value, _defaultFluidModel)) return;
                _defaultFluidModel = value;
                RaisePropertyChanged("DefaultFluidModel");
            }
        }

        public virtual ObservableCollection<NFluidModelGroup> FluidModelGroups
        {
            get { return _fluidModelGroups; }
            set
            {
                if (Equals(value, _fluidModelGroups)) return;
                _fluidModelGroups = value;
                RaisePropertyChanged("FluidModelGroups");
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Возвращает список всех моделей в банке
        /// </summary>
        /// <returns></returns>
        public IEnumerable<NFluidModel> GetAllFluidModels()
        {
            var fluidModels = new List<NFluidModel>();
            foreach (var group in FluidModelGroups)
            {
                fluidModels.AddRange(group.FluidModels);
            }
            return fluidModels;
        }

        /// <summary>
        /// Добавляет модель в заданную группу
        /// </summary>
        /// <param name="fluidModel">Модель жидкости</param>
        /// <param name="group">Группа</param>
        /// <exception cref="InvalidOperationException"></exception>
        public void AddModelToGroup(NFluidModel fluidModel, NFluidModelGroup group)
        {
            var bankGroup = FluidModelGroups.SingleOrDefault(g => g.Equals(group));
            if (bankGroup == null)
                throw new InvalidOperationException(string.Format("Группа '{0}' не найдена в банке", group.Name));

            bankGroup.AddFluidModel(fluidModel);
        }

        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}