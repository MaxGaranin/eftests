﻿using System;
using System.Collections.Generic;

namespace DAL.Classes
{
    public class Survey
    {
        public Survey()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual Client Client { get; set; }
    }

    public class Client
    {
        public Client()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual Survey DefaultSurvey { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }
    }
}