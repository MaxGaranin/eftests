﻿using System;
using System.ComponentModel;

namespace DAL.Classes
{
    public class NFluidCalibration : INotifyPropertyChanged
    {
        private double _value;
        private double _temperature;
        private double _pressure;

        public NFluidCalibration()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public Guid FluidModelId { get; set; }

        public double Value
        {
            get { return _value; }
            set
            {
                if (value.Equals(_value)) return;
                _value = value;
                RaisePropertyChanged("Value");
            }
        }

        public double Temperature
        {
            get { return _temperature; }
            set
            {
                if (value.Equals(_temperature)) return;
                _temperature = value;
                RaisePropertyChanged("Temperature");
            }
        }

        public double Pressure
        {
            get { return _pressure; }
            set
            {
                if (value.Equals(_pressure)) return;
                _pressure = value;
                RaisePropertyChanged("Pressure");
            }
        }

        #region Clone

        public NFluidCalibration Clone(bool isCopyId = false)
        {
            var calibration = new NFluidCalibration
            {
                FluidModelId = this.FluidModelId,
                Value = this.Value,
                Temperature = this.Temperature,
                Pressure = this.Pressure
            };

            if (isCopyId) calibration.Id = this.Id;

            return calibration;
        }

        #endregion
        
        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}