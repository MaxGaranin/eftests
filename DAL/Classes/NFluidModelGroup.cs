﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace DAL.Classes
{
    public class NFluidModelGroup : INotifyPropertyChanged
    {
        private string _name;
        private ObservableCollection<NFluidModel> _fluidModels;

        #region Constructor

        public NFluidModelGroup()
        {
            Id = Guid.NewGuid();
            FluidModels = new ObservableCollection<NFluidModel>();

        }

        #endregion

        #region Properties

        public Guid Id { get; set; }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name == value) return;
                _name = value;
                RaisePropertyChanged("Name");
            }
        }

        public virtual ObservableCollection<NFluidModel> FluidModels
        {
            get { return _fluidModels; }
            private set
            {
                if (Equals(_fluidModels, value)) return;
                _fluidModels = value;
                RaisePropertyChanged("FluidModels");
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Добавляет модель в группу
        /// </summary>
        /// <param name="fluidModel">Модель жидкости</param>
        /// <exception cref="InvalidOperationException"></exception>
        public void AddFluidModel(NFluidModel fluidModel)
        {
            if (_fluidModels.Any(m => m.Name == fluidModel.Name))
                throw new InvalidOperationException();

            _fluidModels.Add(fluidModel);
            fluidModel.Group = this;
            fluidModel.IsExistsInBank = true;
        }

        /// <summary>
        /// Удаляет модель из группы
        /// </summary>
        /// <param name="fluidModel">Модель жидкости</param>
        /// <returns>Флаг успешности удаления</returns>
        public bool RemoveFluidModel(NFluidModel fluidModel)
        {
            var result = _fluidModels.Remove(fluidModel);
            fluidModel.Group = null;
            fluidModel.IsExistsInBank = false;
            return result;
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return Name;
        }

        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}