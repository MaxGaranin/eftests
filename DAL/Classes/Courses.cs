﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Classes
{
    public class Department
    {
        public Department()
        {
            Courses = new HashSet<Course>();
        }

        // Primary key 
        public int DepartmentID { get; set; }
        
        public string Name { get; set; }
        public decimal Budget { get; set; }
        public DateTime StartDate { get; set; }
        public int? Administrator { get; set; }

        // Navigation property 
        public virtual ICollection<Course> Courses { get; private set; }
    }

    public class Course
    {
        public Course()
        {
            Instructors = new HashSet<Instructor>();
        }
        // Primary key 
        public int CourseID { get; set; }

        public string Title { get; set; }
        public int Credits { get; set; }

        // Foreign key 
        public int DepartmentID { get; set; }

        // Navigation properties 
        public virtual Department Department { get; set; }
        public virtual ICollection<Instructor> Instructors { get; private set; }
    }

    public class Instructor
    {
        public Instructor()
        {
            Courses = new List<Course>();
        }

        // Primary key 
        public int InstructorID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime HireDate { get; set; }

        // Navigation properties 
        public virtual ICollection<Course> Courses { get; private set; }
        public virtual OfficeAssignment OfficeAssignment { get; set; }
    }

    public class OfficeAssignment
    {
        // Specifying InstructorID as a primary 
        [Key()]
        public Int32 InstructorID { get; set; }

        public string Location { get; set; }

        // When the Entity Framework sees Timestamp attribute 
        // it configures ConcurrencyCheck and DatabaseGeneratedPattern=Computed. 
        [Timestamp]
        public Byte[] Timestamp { get; set; }

        // Navigation property 
//        public virtual Instructor Instructor { get; set; }
    }
}