﻿using System.Data.Entity.ModelConfiguration;
using DAL.Classes;

namespace DAL.Mapping
{
    public class DepartmentMap : EntityTypeConfiguration<Department>
    {
        public DepartmentMap()
        {
            HasKey(t => t.DepartmentID);
        }
    }

    public class CourseMap : EntityTypeConfiguration<Course>
    {
        public CourseMap()
        {
            HasKey(t => t.CourseID);

            HasRequired(t => t.Department)
                .WithMany(t => t.Courses)
                .WillCascadeOnDelete();

            HasMany(t => t.Instructors)
                .WithMany(t => t.Courses);
        }
    }

    public class InstructorMap : EntityTypeConfiguration<Instructor>
    {
        public InstructorMap()
        {
            HasKey(t => t.InstructorID);

            HasMany(t => t.Courses)
                .WithMany(t => t.Instructors);

            HasRequired(t => t.OfficeAssignment)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();
        }
    }


    public class OfficeAssignmentMap : EntityTypeConfiguration<OfficeAssignment>
    {
        public OfficeAssignmentMap()
        {
            HasKey(t => t.InstructorID);
        }
    }
}