﻿using System.Data.Entity.ModelConfiguration;
using DAL.Classes;

namespace DAL.Mapping
{
    public class ClientMap : EntityTypeConfiguration<Client>
    {
        public ClientMap()
        {
            HasKey(t => t.Id);

            Property(t => t.Name).HasMaxLength(50);

            ToTable("Client");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Name).HasColumnName("Name");

            // Relationships
            // this.HasOptional(t => t.DefaultSurvey)
            //    .WithMany(t => t.Clients).HasForeignKey(d => d.DefaultSurveyId);

            HasOptional(t => t.DefaultSurvey)
                .WithOptionalDependent();
        }
    }

    public class SurveyMap : EntityTypeConfiguration<Survey>
    {
        public SurveyMap()
        {
            HasKey(t => t.Id);

            Property(t => t.Name).HasMaxLength(50);

            ToTable("Survey");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Name).HasColumnName("Name");

            // Relationships
            // this.HasOptional(t => t.Client)
            //      .WithMany(t => t.Surveys).HasForeignKey(d => d.ClientId);
        }
    }
}