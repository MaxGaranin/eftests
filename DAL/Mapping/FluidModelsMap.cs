﻿using System.Data.Entity.ModelConfiguration;
using DAL.Classes;

namespace DAL.Mapping
{
    public class FluidModelBankMap : EntityTypeConfiguration<NFluidModelBank>
    {
        public FluidModelBankMap()
        {
            ToTable("FluidModelBank");

            HasKey(t => t.Id);

            HasMany(t => t.FluidModelGroups)
                .WithRequired()
                .WillCascadeOnDelete();
        }
    }


    public class FluidModelGroupMap : EntityTypeConfiguration<NFluidModelGroup>
    {
        public FluidModelGroupMap()
        {
            ToTable("FluidModelGroup");

            HasKey(t => t.Id);
        }
    }

    public class FluidModelMap : EntityTypeConfiguration<NFluidModel>
    {
        public FluidModelMap()
        {
            ToTable("FluidModel");

            HasKey(t => t.Id);

            HasOptional(t => t.Group)
                .WithMany(t => t.FluidModels)
                .HasForeignKey(t => t.GroupId)
                .WillCascadeOnDelete();

            HasRequired(t => t.AboveOilDensityCalibration)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();            
            
            HasRequired(t => t.AboveOilFvfCalibration)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();

            HasRequired(t => t.AboveUnsaturatedOilCompressibilityCalibration)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();

            HasRequired(t => t.BelowOilDensityCalibration)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();

            HasRequired(t => t.BelowSaturatedOilFvfCalibration)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();

            HasRequired(t => t.GasSaturationCalibration)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();

            HasRequired(t => t.GasViscosityCalibration)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();

            HasRequired(t => t.GasZFactorCalibration)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();

            HasRequired(t => t.SaturatedOilViscosityCalibration)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();
        }
    }

    public class FluidCalibration : EntityTypeConfiguration<NFluidCalibration>
    {
        public FluidCalibration()
        {
            ToTable("FluidCalibration");

            HasKey(t => t.Id);
        }
    }

}