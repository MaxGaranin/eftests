﻿using System;
using System.Collections.Generic;
using DAL.Classes;
using NUnit.Framework;

namespace DAL.Tests
{
    [TestFixture]
    public class CoursesTests
    {
        [Test]
        public void Test()
        {
            using (var dbContext = DataStore.Instance.GetContext())
            {
                var course = new Course { Title = "Course1" };

                var dep = new Department {Name = "Dep1", StartDate = DateTime.Now };
                dep.Courses.Add(course);
                dbContext.Departments.Add(dep);

                var instructor = new Instructor
                {
                    FirstName = "Vasya", 
                    HireDate = DateTime.Now,
                    OfficeAssignment = new OfficeAssignment { Location = "Samara" }
                };
                instructor.Courses.Add(course);
                dbContext.Instructors.Add(instructor);

                dbContext.SaveChanges();
            }

            DataStore.Instance.Save(@"d:\work835\IPA\Work\! Проекты ipax\1.sdf");
        }
    }
}
