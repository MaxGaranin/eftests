﻿using DAL.Classes;
using NUnit.Framework;

namespace DAL.Tests
{
    [TestFixture]
    public class FluidModelBankTests
    {
        [Test]
        public void Test()
        {
            using (var dbContext = DataStore.Instance.GetContext())
            {
                // Log
                // dbContext.Database.Log = s => Debug.WriteLine(s);

                var bank = new NFluidModelBank();
                dbContext.NFluidModelBanks.Add(bank);

                var model = NFluidModel.CreateDefaultModel("Model1", true);
                model.UseAboveCalibrationGroup = true;
                model.AboveOilDensityCalibration.Value = 25;
                model.AboveOilDensityCalibration.Temperature = 2;
                model.AboveOilDensityCalibration.Pressure = 3;

                var group = new NFluidModelGroup { Name = "Group1" };
                group.AddFluidModel(model);

                bank.FluidModelGroups.Add(group);

//                var model2 = NFluidModel.CreateDefaultModel("Model2", true);
//                var group2 = new NFluidModelGroup { Name = "Group2" };
//                group2.AddFluidModel(model2);
//                bank.FluidModelGroups.Add(group2);

                dbContext.SaveChanges();

//                dbContext.NFluidModelGroups.Remove(group);
//                dbContext.SaveChanges();

//                model = new NFluidModel { Name = "Локальная модель" };
//                dbContext.NFluidModels.Add(model);
//                dbContext.SaveChanges();
            }

            DataStore.Instance.Save(@"d:\work835\IPA\Work\! Проекты ipax\1.sdf");          
        }
    }
}