﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Classes;
using DAL.Context;
using NUnit.Framework;

namespace DAL.Tests
{
    [TestFixture]
    public class SurveysTests
    {
        [Test]
        public void Test()
        {
            using (var dbContext = DataStore.Instance.GetContext())
            {
                var client = new Client
                {
                    Name = "Vasya",
                    Surveys = new List<Survey>
                    {
                        new Survey {Name = "SurvayA"},
                        new Survey {Name = "SurvayB"},
                    }
                };

                dbContext.Clients.Add(client);
                dbContext.SaveChanges();
            }

            DataStore.Instance.Save(@"d:\work835\IPA\Work\Проекты ipax\1.sdf");
        }
    }
}
